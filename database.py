#!/usr/bin/env python

#-----------------------------------------------------------------------
# database.py
# Author: Rod Eric Joseph and Traci Mathieu
# Inspired by Robert Dondero's database.py
#-----------------------------------------------------------------------

from sqlite3 import connect, Error
import sqlite3
from sys import stderr
from os import path
from result import Result

#-----------------------------------------------------------------------

class Database:

    def __init__(self):
        self._connection = None

    def connect(self):
        DATABASE_NAME = 'reg.sqlite'
        if not path.isfile(DATABASE_NAME):
            raise Exception('Database connection failed')
        self._connection = connect(DATABASE_NAME)

    def disconnect(self):
        self._connection.close()

    def process_results_details(self, cursor, statements):

        row = cursor.fetchone()

        courseid = str(row[0])
        days = str(row[1])
        starttime = str(row[2])
        endtime = str(row[3])
        bldg = row[4]
        roomnum = row[5]

        try:
            cursor.execute(statements["courses"], [courseid])
        except Exception as e:
            print >> stderr, 'database: ' + str(e)
            return "database: " + str(e) + " could not be found"

        row = cursor.fetchone()
        area = row[0]
        title = row[1]
        descript = row[2]
        prereqs = row[3]

        profs = []
        profids = []
        depts = []
        coursenums = []

        try:
            cursor.execute(statements["crosslistings"], [courseid])
        except Exception as e:
            print >> stderr, 'database: ' + str(e)
            return "database: " + str(e) + " could not be found"

        row = cursor.fetchone()
        while row:
            dept = row[0]
            coursenum = dept + " " + row[1]
            if dept not in depts:
                depts.append(dept)
            if coursenum not in coursenums:
                coursenums.append(coursenum)
            row = cursor.fetchone()

        try:
            cursor.execute(statements["coursesprofs"], [courseid])
        except Exception as e:
            print >> stderr, 'database: ' + str(e)
            return "database: " + str(e) + " could not be found"

        row = cursor.fetchone()
        while row:
            profid = row[0]
            if profid not in profids:
                profids.append(profid)
            row = cursor.fetchone()

        for profid in profids:
            try:
                cursor.execute(statements["profs"], [profid])
                row = cursor.fetchone()
                while row:
                    prof = row[0]
                    if prof not in profs:
                        profs.append(prof)
                    row = cursor.fetchone()
            except Exception as e:
                print >> stderr, 'database: ' + str(e)
                return "database: " + str(e) + " could not be found"

        details = { "courseid": courseid, "days": days, "starttime": starttime,
        "endtime": endtime, "bldg": bldg, "roomnum": roomnum, "area": area, "title": title,
        "descript": descript, "prereqs": prereqs, "profs": profs, "depts": depts,
        "coursenums": coursenums }

        return details

    def process_results(self, cursor):
        classes = []
        row = cursor.fetchone()
        while row:
            classid = str(row[0])
            if len(classid) == 4:
                classid = classid + " "
            dept = str(row[1])
            coursenum = str(row[2])
            if len(coursenum) == 3:
                coursenum = coursenum + " "
            area = str(row[3])
            title = str(row[4])
            if area == "":
                area = "   "
            if len(area) == 2:
                area = area + " "
            newClass = Result(classid, dept, coursenum, area, title)
            classes.append(newClass)
            row = cursor.fetchone()
        return classes

    def get_classes(self, dept, area, coursenum, title):
        cursor = self._connection.cursor()
        keys = {"dept": dept, "area": area, "coursenum": coursenum, "title": title}

        for key in keys:
            value = keys[key]
            if value.find("%") != -1:
                value = value.replace("%", r"\%")
            elif value.find("_") != -1:
                value = value.replace("_", r"\_")
            keys[key] = "%" + value + "%"

        dept = keys["dept"]
        area = keys["area"]
        coursenum = keys["coursenum"]
        title = keys["title"]

        QUERY_STRING = "SELECT classid, dept, coursenum, area, title, courses.courseid " + \
        "FROM courses, classes, crosslistings " + \
        "WHERE courses.courseid = classes.courseid " + \
        "AND courses.courseid = crosslistings.courseid " + \
        "AND (coursenum LIKE ?) AND (area LIKE ?) AND (title LIKE ?) AND (dept LIKE ?) " + \
        "ORDER BY dept, coursenum, title"

        try:
           cursor.execute(QUERY_STRING, (coursenum, area, title, dept))
        except Exception as e:
           print >> stderr, 'database.py: ' + str(e)
           return e

        results = self.process_results(cursor)

        return results

    def get_details(self, classid):

        cursor = self._connection.cursor()

        # Check that the classid exists
        stmtStr = 'SELECT courseid from classes WHERE classid = ?'
        try:
            cursor.execute(stmtStr, [classid])
        except Exception as e:
            print >> stderr, 'database: ' + str(e)
            return 'database: ' + str(e)
        data = cursor.fetchone()
        if data is None:
            print >> stderr, 'database: classid does not exist'
            return 'database: classid does not exist'

        stmt_classes = "SELECT DISTINCT courseid, days, starttime, endtime, bldg, roomnum " + \
        "FROM classes WHERE classid = ?"

        stmt_courses = "SELECT DISTINCT area, title, descrip, prereqs " + \
        "FROM courses WHERE courseid = ?"

        stmt_crosslistings = "SELECT DISTINCT dept, coursenum " + \
        "FROM crosslistings WHERE courseid = ? " + \
        "ORDER BY dept, coursenum"

        stmt_coursesprofs = "SELECT DISTINCT profid " + \
        "FROM coursesprofs WHERE courseid = ?"

        stmt_profs = "SELECT DISTINCT profname FROM profs WHERE profid = ?"

        statements = {"classes": stmt_classes, "courses": stmt_courses, "crosslistings": stmt_crosslistings, "coursesprofs": stmt_coursesprofs, "profs": stmt_profs}

        try:
            cursor.execute(statements["classes"], [classid])
        except Exception as e:
            print >> stderr, 'database: ' + str(e)
            return "database: " + str(e) + " could not be found"

        results = self.process_results_details(cursor, statements)
        return results

#-----------------------------------------------------------------------

# For testing:

if __name__ == '__main__':
    database = Database()
    database.connect()
    classes = database.get_classes("COS", "", "", "")
    database.disconnect()
