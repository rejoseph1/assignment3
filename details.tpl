<h2>Class Details (class id {{classid}})</h2>
<table>
  <tr>
    <td align="left"><strong>Course Id:</strong> {{results["courseid"]}}</td>
  </tr>
  <tr>
    <td align="left"><strong>Days:</strong> {{results["days"]}}</td>
  </tr>
  <tr>
    <td align="left"><strong>Start time:</strong> {{results["starttime"]}}</td>
  </tr>
  <tr>
    <td align="left"><strong>End time:</strong> {{results["endtime"]}}</td>
  </tr>
  <tr>
    <td align="left"><strong>Building:</strong> {{results["bldg"]}}</td>
  </tr>
  <tr>
    <td align="left"><strong>Room:</strong> {{results["roomnum"]}}</td>
  </tr>
</table>

<hr>

<h2>Class Details (course id {{results["courseid"]}})</h2>
<table>
  % for coursenum in results["coursenums"]:
  <tr>
    <td align="left"><strong>Dept and Number:</strong> {{coursenum}}</td>
  </tr>
  % end
  <tr>
    <td align="left"><strong>Area: </strong>{{results["area"]}}</td>
  </tr>
  <tr>
    <td align="left"><strong>Title: </strong>{{results["title"]}}</td>
  </tr>
  <tr>
    <td align="left"><strong>Description: </strong>{{results["descript"]}}</td>
  </tr>
  <tr>
    <td align="left"><strong>Prerequisites: </strong>{{results["prereqs"]}}</td>
  </tr>
  <tr>
    <td align="left"><strong>Professor(s): </strong>
  % for prof in results["profs"]:
      {{prof}},
  % end
    </td></tr>
</table>
