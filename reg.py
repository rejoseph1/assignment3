#!/usr/bin/env python

#-----------------------------------------------------------------------
# reg.py
# Author: Rod Eric Joseph and Traci Mathieu
#-----------------------------------------------------------------------

from sys import argv
from database import Database
from time import localtime, asctime, strftime
from urllib import quote_plus
from bottle import route, request, response, error, redirect, run
from bottle import template, TEMPLATE_PATH
from Cookie import SimpleCookie

TEMPLATE_PATH.insert(0, '')

#may need to change reg3 to index
@route('/')
@route('/reg3')
def index():

    dept = request.query.get("dept")
    coursenum = request.query.get("coursenum")
    area = request.query.get("area")
    title = request.query.get("title")

    # Replace "None" with empty String, if necessary
    if dept == None:
        dept = ""
    if coursenum == None:
        coursenum = ""
    if area == None:
        area = ""
    if title == None:
        title = ""

    database = Database()
    database.connect()
    results = database.get_classes(dept, area, coursenum, title)
    database.disconnect()
    templateInfo = {"results": results, "dept": dept,
    "coursenum": coursenum, "area": area, "title": title}

    response.set_cookie('prevDept', dept)
    response.set_cookie('prevCoursenum', coursenum)
    response.set_cookie('prevArea', area)
    response.set_cookie('prevTitle', title)

    return template('reg3.tpl', templateInfo)

@route('/reg3/regdetails')
def regdetails():
    classid = request.query.get("classid")
    if classid == "" or classid == None:
        return template("error.tpl", {"errorMessage": "Missing class id"})

    try:
        int(classid)
    except ValueError:
        return template("error.tpl", {"errorMessage": "Class id is not numeric"})

    database = Database()
    database.connect()
    results = database.get_details(classid)

    # Print the error message on the page
    if type(results) is str:
        return template("error.tpl", {"errorMessage": results})

    database.disconnect()

    cookienames = ['prevDept', 'prevCoursenum', 'prevArea', 'prevTitle']
    cookies =[]
    for cookie in cookienames:
        s = request.get_cookie(cookie)
        if s == None:
            cookies.append("")
        else:
            cookies.append(s)

    templateInfo = {"classid": classid, "results": results, "prevDept": cookies[0],
                    "prevCoursenum": cookies[1], "prevArea": cookies[2], "prevTitle": cookies[3]}
    for cookie in cookies:
        print cookie
    return template('regdetails.tpl', templateInfo)

@error(404)
def notFound(error):
    return 'Not found'

if __name__ == '__main__':
    if len(argv) != 2:
        print 'Usage: ' + argv[0] + ' port'
        exit(1)
    run(host='0.0.0.0', port=argv[1], debug=True)
