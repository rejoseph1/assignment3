<!DOCTYPE html>
<html>
  <head>
    <title> Registrar's Office Class Search </title>
  </head>
  <body>
    <h1> Registrar's Office </h1>
    <h2> Class Search </h2>
    <hr>
    % include('searchform.tpl')
    <hr>
    % include('searchresults.tpl')
    % include('footer.tpl')
  </body>
</html>
