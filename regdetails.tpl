<!DOCTYPE html>
<html>
  <head>
    <title> Registrar's Office Class Details </title>
  </head>
  <body>
    <h1> Registrar's Office </h1>
    <hr>
    % include('details.tpl')
    <hr>
    Click here to do <a href="/reg3?dept={{ prevDept }}&coursenum={{ prevCoursenum }}&area{{ prevArea }}=&title={{ prevTitle }}">another class search</a>
    % include('footer.tpl')
  </body>
</html>
