#!/usr/bin/env python

#-----------------------------------------------------------------------
# result.py
# Authors: Rod Eric Joseph and Traci Mathieu
#-----------------------------------------------------------------------

class Result (object):

    def __init__(self, classid, dept, coursenum, area, title):
        self._classid = classid
        self._dept = dept
        self._coursenum = coursenum
        self._area = area
        self._title = title

    def __str__(self):
        return self._classid + ', ' + self._dept + ', ' + \
        str(self._coursenum) + ', ' + self._area + ', ' + self._title

    def setClassid(self, classid):
        self._classid = classid

    def getClassid(self):
        return self._classid

    def setDept(self, dept):
        self._dept = dept

    def getDept(self):
        return self._dept

    def setCoursenum(self, coursenum):
        self._coursenum = coursenum

    def getCoursenum(self):
        return self._coursenum

    def setArea(self, area):
        self._area = area

    def getArea(self):
        return self._area

    def setTitle(self, title):
        self._title = title

    def getTitle(self):
        return self._title
