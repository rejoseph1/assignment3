<table>
  <tr>
  <td align="left"><strong>ClassId</strong></td>
  <td align="left"><strong>Dept</strong></td>
  <td align="left"><strong>Num</strong></td>
  <td align="left"><strong>Area</strong></td>
  <td align="left"><strong>Title</strong></td>
  </tr>
% for result in results:
  <tr>
  <td align="left"><a href="reg3/regdetails?classid={{ result.getClassid() }}">{{result.getClassid()}}</a></td>
  <td align="left">{{result.getDept()}}</td>
  <td align="left">{{result.getCoursenum()}}</td>
  <td align="left">{{result.getArea()}}</td>
  <td align="left">{{result.getTitle()}}</td>
  </tr>
% end
</table>
